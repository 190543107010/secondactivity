package com.example.secondactivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class second_activity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent i= getIntent();
        String d = i.getExtras().getString("td","");

        TextView t =findViewById(R.id.txfinal);
        t.setText(d);
    }
}
