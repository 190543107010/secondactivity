package com.example.secondactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn= findViewById(R.id.btactsubmit);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText t1=findViewById(R.id.etactfasnm);
                EditText t2=findViewById(R.id.etactlasnm);
                String tx1 = t1.getText().toString();
                String tx2 = t2.getText().toString();
                String tmp = tx1+" "+tx2;
                Intent i =new Intent(MainActivity.this,second_activity.class);
                i.putExtra("td",tmp);
                startActivity(i);
            }
        });
    }
}
